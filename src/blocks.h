// Blocks v. 0.51 Alpha Class
//
// Versao experimental
// classes genericas de controle de blocos e tabuleiros bidimensionais
//
// Ultima versao: 11/03/2001, 19:30 PM
//
// A ser implementado nesta versao:
//
// * Circularidade de tabuleiros - OK!
// * Movimentacao angular das pecas - com erros
//
//
// Funcoes de Bresenham modificadas: BresLineMove e BresBlockMove
// Programador original:  Kenny Hoff
// Data:        10/25/95


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <stdio.h>
#include <vector>
#include <string>
#include <math.h>
#include <time.h>
#include <iostream.h>




using namespace std;


#define PI 3.141592 // 180 GRAUS
#define ZERO 0 // 0 GRAUS
#define HALF_PI 1.57 // 90 GRAUS
#define THREE_HALF_PI 0.78 //45 GRAUS
#define PI_HALF_PI 4.72 // 270 GRAUS
#define PI_THREE_HALF_PI 5.49 // 315 GRAUS



#define FRAMES 4 // numero de frames de um framedblock
#define DEFSIZEX 69 // tamanho do tabuleiro (x, y)
#define DEFSIZEY 22
#define FREE 0		// status dos 'pieces'
#define OK 1
#define SIZEX 10// tamanho das pecas
#define SIZEY 10


using namespace std;


class tabuleiro;



// Classe piece
//
//////////////////////////////////////////////////////////////////////

#if !defined (PIECE)
#define PIECE


class piece
{
public:
	piece();
	bool Line(int x1, int x2, int x3, int x4);
	virtual ~piece();
	void setStatus(int x);
	int getStatus();
	void setTabuleiro(tabuleiro *tab); // seta o tabuleiro da peca
	tabuleiro * getTabuleiro(); // retorna o tabuleiro da peca
	void place(int x, int y); //coloca peca no tabuleiro
	int getIcon();
	void setIcon(int x);
	bool move(int quantX, int quantY);
	bool move(int length); // movimentacao com um angulo
	int getX();
	int getY();
	void setX(int x);
	void setY(int y);
	void setFacing(float ang);
	double getFacing();


private:
	bool BresLineMove(double Ax, double Ay, double Bx, double By); // Bresenham (move em uma linha... retorna true se chegou ao fim)

	tabuleiro * view;
	int status;
	int situation;
	int x, y;
	double facing; // angulo (em radianos)
	int icon; // um representativo qualquer... cor, talvez??

};

#endif





// Classe tabuleiro
//
//////////////////////////////////////////////////////////////////////

#if !defined (TABULEIRO)
#define TABULEIRO

class tabuleiro  
{
public:

	void removeAt(int x, int y);
	bool PosicaoOcupada(int x, int y);
	tabuleiro(int x = DEFSIZEX, int y = DEFSIZEY);
	virtual ~tabuleiro();
	void draw(int xinic = 0, int yinic = 0);
	void place(piece *pec);
	void init_tab();
	void remove(piece *pec);
	void setCircular(bool is);
	bool isCircular();
	int tab[DEFSIZEX][DEFSIZEY];

protected:
	int sizex, sizey;
	vector<piece *> eixo; // todas as pecas 'renderable'
	
	bool circular;


};


#endif

// classe block
//
//////////////////////////////////////////////////////////////////////


#if !defined (BLOCK)
#define BLOCK

class block
{
public:
	tabuleiro * getTabuleiro();
	int getLength();
	void setPos(int x, int y);
	void setIcon(int x);
	void setStatus(int x);
	int getStatus(); // status da peca (armazenado em SITUATION...)
	int getHeight();
	int getX();
	int getY();
	void setSize();
	bool move(int quantidade); // movimentacao com angulos
	bool move(int qx, int qy);
	void setTabuleiro(tabuleiro *t);
	block();
	void setFacing(double fac);
	double getFacing();
	virtual ~block();
	void setPiece(int posx, int posy);
	void unsetPiece(int posx, int posy);
	void place(int x, int y);
	void remove();

	piece blocos[SIZEX][SIZEY];	

private:
	bool BresBlockMove(int x, int y, int x2, int y2);
	double facing;
	int situation;
	int xpos, ypos;
	int comprimento;
	int altura;

};

#endif


// classe framedblock
//
//////////////////////////////////////////////////////////////////////


#if !defined (FRAMEDBLOCK)
#define FRAMEDBLOCK


class framedblock
{
public:
	framedblock();
	virtual ~framedblock();

	int getX();
	int getY();
	bool move(int x, int y);
	void setTabuleiro(tabuleiro *tab);
	void place(int x, int y);
	bool rotate();
	block * atual;
	

protected:
	int actual_facing;
	block quadro[FRAMES];


};

#endif
