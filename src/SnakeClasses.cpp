// SNAKECLASSES - classes genericas para o jogo Snake

// Programador: Herval Freire de A. Júnior
//
// uso das classes Block como base e tabuleiro, cobra e tabuleiro descritos em snake.h
// Usando o CDX 3.0 para acesso aos recursos DirectX
//
// versao 0.1a
// ultima alteracao: 30/03/2001, 17:45 PM

#include "SnakeClasses.h"


// classe SnakeGround
//
//////////////////////////////////////////////////////////////////////


Snakey::Snakey(CDXScreen *Tela, SnakeGround * tabul, int xi, int yi, int size)
{


tela = Tela;

x_tab = tabul->getX();

y_tab = tabul->getY();

Cobra = new CDXSurface();

if(Cobra->Create(Tela, "body.bmp")<0)
		CDXError(NULL, "Impossível abrir figuras");

	if(Cobra->SetColorKey() < 0)
		CDXError(NULL, "ERRO");


cobrinha = new snake(tabul->getTabuleiro(), xi, yi, size);


}







Snakey::~Snakey()
{



	tela = NULL;




}




void Snakey::place(int x, int y)
{

	this->cobrinha->place(x,y);
	

}


void Snakey::setTabuleiro(SnakeGround *tabul)
{
	this->cobrinha->setTabuleiro(tabul->getTabuleiro());
	this->x_tab = tabul->getX();
	this->y_tab = tabul->getY();
}






bool Snakey::moveFront()
{
	
	return cobrinha->moveFront();

}


bool Snakey::moveLeft()
{

	return cobrinha->move(cobrinha->getX()-1, cobrinha->getY());

}



bool Snakey::moveUp()
{

	return cobrinha->move(cobrinha->getX(), cobrinha->getY()-1);

}


bool Snakey::moveRight()
{

	return cobrinha->move(cobrinha->getX()+1, cobrinha->getY());
}


bool Snakey::moveDown()
{

	return cobrinha->move(cobrinha->getX(), cobrinha->getY()+1);
}


void Snakey::RenderAt(int x, int y, float scale)
{
	RECT ret = {0,0,XSPACING,YSPACING};

	// nao implementado

}

void Snakey::Render()
{
	
	int posX = cobrinha->getX();
	int posY = cobrinha->getY();

	RECT ret = {0,0,XSPACING,YSPACING};

	vector<piece>::iterator it;

	for(it = cobrinha->body.begin(); it != cobrinha->body.end(); it++)
	{
		
		ret.left = 0;
		ret.right = XSPACING;
	
		
/*		if(it->getStatus() == CAUDA)
		{
			ret.left = 0;
			ret.right = XSPACING;		

			if(it->getIcon() == FACE_UP)
			{
				Cobra->DrawTrans(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
			if(it->getIcon() == FACE_DOWN)
			{
				Cobra->DrawTransVFlip(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
			
			if(it->getIcon() == FACE_LEFT)
			{
				ret.left += XSPACING;
				ret.right += XSPACING;
				Cobra->DrawTrans(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
			if(it->getIcon() == FACE_RIGHT)
			{
				ret.left += XSPACING;
				ret.right += XSPACING;
				Cobra->DrawTransHFlip(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
		}
		if(it->getStatus() == CABECA)
		{
			ret.left = 2*XSPACING;
			ret.right = 3*XSPACING;		
	

			if(it->getIcon() == FACE_UP)
			{
				Cobra->DrawTrans(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
			if(it->getIcon() == FACE_DOWN)
			{
				Cobra->DrawTransVFlip(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
			if(it->getIcon() == FACE_LEFT)
			{
				ret.left += XSPACING;
				ret.right += XSPACING;
				Cobra->DrawTrans(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
			if(it->getIcon() == FACE_RIGHT)
			{
				ret.left += XSPACING;
				ret.right += XSPACING;
				Cobra->DrawTransHFlip(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
		}
		if(it->getStatus() == CORPO)
		{
			ret.left = 4*XSPACING;
			ret.right = 5*XSPACING;		

			if((it->getIcon() == FACE_UP) || (it->getFacing() == FACE_DOWN))
			{ // OK
				Cobra->DrawTrans(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
			if((it->getIcon() == FACE_LEFT) || (it->getFacing() == FACE_RIGHT))
			{
				ret.left += XSPACING;
				ret.right += XSPACING; // OK
				Cobra->DrawTrans(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
		}
		if(it->getStatus() == CORPOL)
		{
			ret.left = 6*XSPACING;
			ret.right = 7*XSPACING;		

			if(it->getIcon() == FACE_RIGHT)
			{
				Cobra->DrawTransHFlip(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
			if(it->getIcon() == FACE_LEFT)
			{
				Cobra->DrawTransHFlip(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
			if(it->getIcon() == FACE_DOWN)
			{
				ret.left+=XSPACING; // OK
				ret.right+=XSPACING;
				Cobra->DrawTransHFlip(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
			if(it->getIcon() == FACE_UP)
			{
				Cobra->DrawTransHFlip(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);
			}
		}
*/
		Cobra->DrawBlk(tela->GetBack(), (it->getX()*XSPACING)+x_tab, (it->getY()*YSPACING)+y_tab, &ret);

	} // fim do FOR


}





// SnakeGround: implementacao
//
//////////////////////////////////////////////////////////////////////


SnakeGround::SnakeGround(CDXScreen * Tela, int xi, int yi, int min_apples)
{

	Xinic = xi;
	Yinic = yi;


//score = new CDXBitmapFont();

pecas = new CDXTile();

TabDrawing = NULL;

this->tela = Tela;


if(pecas->Create(Tela, "body.bmp", 24, 24, 8)<0)
		CDXError(NULL, "Impossível abrir figuras");

	if(pecas->SetColorKey() < 0)
		CDXError(NULL, "ERRO");
//score->CreateFromFile(Tela, "font.bhf", 32, 32, ' ', 60);


tabu = new snaketab(min_apples);


}


void SnakeGround::setBack(char * filename)
{

TabDrawing = new CDXSurface();

if(TabDrawing->Create(tela, filename)<0)
		CDXError(NULL, "Impossível abrir Fundo");

}


SnakeGround::~SnakeGround()
{

	SAFEDELETE(TabDrawing);

	
	SAFEDELETE(pecas);

}



snaketab * SnakeGround::getTabuleiro()
{ 
	return tabu;
}


int SnakeGround::getX()
{

return Xinic;
}


int SnakeGround::getY()
{

return Yinic;
}



/*void SnakeGround::DisplayTotalElims(int x, int y)
{


char buffer[10];

_itoa(tabu->getTotalElim(), buffer, 10);


score->DrawTrans(x,y, buffer, tela->GetBack());



}

*/

void SnakeGround::Draw()
{

	if(TabDrawing)
		TabDrawing->DrawBlk(tela->GetBack(), getX(), getY());

	RECT ret = {0,0,XSPACING,YSPACING};


	for(int i=0; i<DEFSIZEX; i++)
		for(int j=0; j<DEFSIZEY; j++)
		if(tabu->tab[i][j] != FREE)
			if((tabu->tab[i][j] != CABECA) && (tabu->tab[i][j] != CAUDA) && (tabu->tab[i][j] != CORPO) && (tabu->tab[i][j] != CORPOL) && (tabu->tab[i][j] != CORPOLI))
			{
				ret.left = (tabu->tab[i][j]-1)*XSPACING;
				ret.right =((tabu->tab[i][j]-1)*XSPACING)+XSPACING;
				pecas->DrawBlk(tela->GetBack(), getX()+(XSPACING*i), getY()+(YSPACING*j), &ret);
			}
				



	

}



