// SNAKECLASSES - classes genericas para o jogo Snake

// Programador: Herval Freire de A. J�nior
//
// uso das classes Block como base e tabuleiro, cobra e tabuleiro descritos em snake.h
// Usando o CDX 3.0 para acesso aos recursos DirectX
//
// versao 0.1a
// ultima alteracao: 30/03/2001, 17:45 PM


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define CDXINCLUDEALL
#include <cdx.h>

#include "blocks.h"
#include "snake.h"
//#include "addons/CDXBitmapFont.h"




#define XSPACING 24
#define YSPACING 24




// classe SnakeGround: mapeamento do chao
//
//////////////////////////////////////////////////////////////////////

#if !defined(SNAKEGROUND)
#define SNAKEGROUND



class SnakeGround
{
public:
	SnakeGround(CDXScreen * tela, int xi, int yi, int min_apples = 1); // recebe o CDXScreen onde pode escrever
	virtual ~SnakeGround();
	void setBack(char *filename);
	snaketab * getTabuleiro();
	int getX();
	int getY();
	void Draw();
	
	

private:

	snaketab *tabu;
	CDXTile * pecas;
//	CDXBitmapFont * score;
	CDXScreen * tela;
	CDXSurface * TabDrawing;
	int Xinic;
	int Yinic;


};

#endif



// classe TetrisBlock: bloco generico
//
//////////////////////////////////////////////////////////////////////

#if !defined(SNAKEY)
#define SNAKEY


class Snakey
{
public:
	void place(int x, int y);
	bool moveFront();
	bool moveDown();
	bool moveUp();
	bool moveRight();
	bool moveLeft();
	Snakey(CDXScreen * tela, SnakeGround * tabul, int xi, int yi, int init_size = 1);
	void setTabuleiro(SnakeGround *tabul);
	virtual ~Snakey();
	void RenderAt(int x, int y, float scale);
	
	void Render();

protected:
	snake * cobrinha;
	
	CDXSurface *Cobra;
	CDXScreen *tela;

	int x_tab;
	int y_tab;
};

#endif

