// Cobrinha v. 0.5 Alpha Class
//
// Versao experimental
// classe de implementacao do jogo cobrinha
//
// Ultima versao: 30/03/2001, 17:45 PM
//
//






#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "blocks.h"


#define CAUDA 1 // ahn... cauda??
#define CABECA 2 // cabeca!! :P
#define CORPO 3 // corpo
#define CORPOL 4 // corpo (juncao em L)
#define CORPOLI 5 // corpo (juncao em L invertido)


#define APPLE 9 // ma��

#define FACE_LEFT 1
#define FACE_RIGHT 2
#define FACE_UP 3
#define FACE_DOWN 4

/*#define H 1// horizontal   ->
#define V 2// vertical		para baixo
#define VI 3//vertical invertido     para cima
#define HI 4// horizontal invertido    <-
*/





////////////////////////////////////////////////////////////////////////////
// Classe snaketab
////////////////////////////////////////////////////////////////////////////

#if !defined(SNAKETAB)
#define SNAKETAB

class snaketab: public tabuleiro
{
public:
	snaketab(int min_macas = 1);
	virtual ~snaketab();
	void checkHarvest(); // ve se tem menos macas que o minimo
	void setApple();
	void removeApple(int x, int y);	
	bool IsAppleAt(int x, int y);

private:
	int min_macas;	
	int macas;

};

#endif









////////////////////////////////////////////////////////////////////////////
// Classe snake
////////////////////////////////////////////////////////////////////////////



#if !defined(SNAKE)
#define SNAKE

class snake  
{
public:
	snake(snaketab *tab, int xi = 0, int yi = 0, int size = 0);
	void Kill();
	int getX();
	int getY();
	virtual ~snake();
	void place(int x, int y);
	void remove();
	bool move(int qx, int qy);
	bool moveFront();
	snaketab * getTabuleiro();
	void setTabuleiro(snaketab *tab);
	int getEatedTotal();
	piece * getHead();

	vector<piece> body;
	

protected:

	void IncreaseBody();
	bool canEat();
	bool canEat(int x, int y);
	void EatApple();
	void EatApple(int x, int y);


	int eated;

	snaketab * ground;


};

#endif




