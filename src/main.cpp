// SNAKE versao 0.5a
// Ultima atualizacao: 12/03/2001, 23:00 PM
//
// programa principal



#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <windowsx.h>
#include <stdio.h>


#include "SnakeClasses.h"





#define GAMEOVER -1
#define BEGIN 1
#define INIT 0
#define PAUSE 2



char AppName[] = "SNAKE version 0.5 Alpha Class";
char Classname[] = "Herv.Snake";

HINSTANCE hInst;
HWND hWnd;


CDXScreen * tela;

Snakey * cobra1;

CDXSurface * fundo;


SnakeGround * chao;


int delay = 0;
int mus_id = 0;
int state = 0;





/////////////////////////////////////////////////////////////////

void paused()
{
	

	state = PAUSE;

	
	chao->Draw();




	tela->Flip();

	
}

/////////////////////////////////////////////////////////////////

void game_over()
{

	state = GAMEOVER;


	chao->Draw();



	tela->Flip();




}

//////////////////////////////////////////////



void finaliza_CDX()
{

	SAFEDELETE(fundo);

	SAFEDELETE(tela);

}


////////////////////////////////////////////////////////////


long PASCAL WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
switch(state)
{
	case BEGIN:
	switch(message)
	{

		case WM_KEYDOWN:switch(wParam)
			            {
				            case VK_ESCAPE: // if ESC key was hit, quit program
								tela->FadeToBlack(2000);
								finaliza_CDX();
								PostQuitMessage(0);
								break;
 				            break;
							case VK_LEFT: if(cobra1->moveLeft() == false) state = GAMEOVER; break;
							case VK_RIGHT: if(cobra1->moveRight() == false) state = GAMEOVER; break;
							case VK_UP: if(cobra1->moveUp() == false) state = GAMEOVER; break;
							case VK_DOWN: if(cobra1->moveDown() == false) state = GAMEOVER; break;
							case VK_PAUSE: paused(); break;
				          }
		                break;

		case WM_DESTROY:    
			                PostQuitMessage(0); // terminate the program
		                    break;
		case WM_TIMER: 
			if(cobra1->moveFront() == false) state = GAMEOVER;
			chao->getTabuleiro()->checkHarvest();
					break;
		
		
	}
	break;
	

	case PAUSE: switch(message)
				{
				case WM_KEYDOWN: if(wParam==VK_PAUSE) 
					{
		//			sound_switch();
					state = BEGIN;
					break;
					}
				}
	break;
	
	
	case GAMEOVER: switch(message)
				   {
					case WM_KEYDOWN: if(wParam == VK_ESCAPE)
									 {
										tela->FadeToBlack(2000);
										finaliza_CDX();
										PostQuitMessage(0);
									 }
									break;
				   }
	break;

}
	return DefWindowProc(hWnd, message, wParam, lParam);
}



/////////////////////////////////////////////////////////////////



BOOL Inicia_CDX()
{


	

	tela = new CDXScreen();
	if(tela==NULL)
		CDXError(NULL, "Imposs�vel criar CDXScreen");


	if(tela->CheckIfVideoModeExists(800,600,8) == TRUE)
	{
//		if(tela->CreateWindowed(hWnd, 800, 600) != FALSE)
		if(tela->CreateFullScreen(hWnd, 800, 600, 16) != FALSE)
			CDXError(NULL, "Imposs�vel inicializar v�deo");
	}

	
	if(tela->LoadPalette("body.bmp")<0)
		CDXError(NULL, "Imposs�vel abrir paleta");


	
	fundo = new CDXSurface();

		if(fundo->Create(tela, "ground.bmp") < 0)
			CDXError(NULL, "Impossivel abrir fundo");

	//figura->SetColorKey();

		

	chao = new SnakeGround(tela, 40, 74, 2);

	cobra1 = new Snakey(tela, chao, 5, 5, 1);
	


return TRUE;

}

/////////////////////////////////////////////////////////////////


static void Ajusta_janela()
{
	RECT rect = {0, 0, 800, 600};
	DWORD dwStyle;

	dwStyle = GetWindowStyle(hWnd);
	dwStyle &= ~WS_POPUP;
	dwStyle |= WS_OVERLAPPED | WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU;

	SetWindowLong(hWnd, GWL_STYLE, dwStyle);


	AdjustWindowRectEx(&rect, GetWindowStyle(hWnd), GetMenu(hWnd) != NULL, GetWindowExStyle(hWnd));

	SetWindowPos(hWnd, NULL, 0, 0, rect.right-rect.left, rect.bottom-rect.top, SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);

	SetWindowPos(hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE);

}



/////////////////////////////////////////////////////////////////


void roda_cena()
{


	tela->Fill(0);
	
	fundo->DrawBlkStretched(tela->GetBack(), 0, 0, NULL, 800,600);
	
	chao->Draw();

	cobra1->Render();

	tela->Flip();

}


/////////////////////////////////////////////////////////////////


BOOL Inicia(int nCmdShow)
{

	WNDCLASS WndClass;

	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.lpfnWndProc = WinProc;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.hInstance = hInst;
	WndClass.hIcon = LoadIcon(0, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(0, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.lpszMenuName = AppName;
	WndClass.lpszClassName = Classname;
	RegisterClass(&WndClass);

    // create a window which covers the whole screen
    // this is needed for fullscreen CDX apps
	hWnd = CreateWindowEx(
		WS_EX_TOPMOST,
		Classname,
		AppName,
		WS_POPUP,
		0,0,
		GetSystemMetrics(SM_CXFULLSCREEN),
		GetSystemMetrics(SM_CYFULLSCREEN),
		NULL,
		NULL,
		hInst,
		NULL);

    // when hWnd = -1 there was an error creating the main window
    // CDXError needs a CDXScreen object, if there is none at this early
    // program stage, pass it NULL
	if(!hWnd) 
        CDXError( NULL , "could not create the main window" );


   
//	Ajusta_janela();
	// show the main window
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);



	SetTimer(hWnd, 1, 500, NULL);

	state = BEGIN;

	return TRUE;
}



/////////////////////////////////////////////////////////////////


int PASCAL WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	
	MSG msg;

	if(!Inicia(nCmdShow))
        CDXError( NULL , "could not initialize CDX application" );

	if(!Inicia_CDX())
	{
		PostQuitMessage(0);
		return FALSE;
	}



	while(1)
	{
		if(PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		{
			if(!GetMessage(&msg, NULL, 0, 0 )) return msg.wParam;
			TranslateMessage(&msg); 
			DispatchMessage(&msg);
		}
			
		else {

			if(state == BEGIN)
			{
				roda_cena();

			}

			if(state == PAUSE)
			{
				paused();
				
			}

			if(state == GAMEOVER)
			{
				game_over();
				
			}

			}
	}
}
