// Cobrinha v. 0.5 Alpha Class
//
// Versao experimental
// classe de implementacao do jogo cobrinha
//
// Ultima versao: 30/03/2001, 17:45 PM
//
//

// classe snake
//
//////////////////////////////////////////////////////////////////////

#include "snake.h"


snake::snake(snaketab *tab, int xi, int yi, int size)
{

	body.clear();

	eated = 0;

	ground = tab;

piece * cabecao = new piece();
	cabecao->setX(xi);
	cabecao->setY(yi);
	cabecao->setStatus(CABECA);
	cabecao->setIcon(FACE_UP);
	cabecao->setTabuleiro(tab);


piece * cauda = new piece();
	cauda->setX(xi);
	cauda->setY(yi+size+1);
	cauda->setStatus(CAUDA);
	cauda->setIcon(FACE_UP);
	cauda->setTabuleiro(tab);


	body.push_back(*cabecao); // a cabeca eh a primeira peca!!
	

	body.push_back(*cauda); // cauda eh a ultima

for(size; size>0; size--)
	{
	IncreaseBody();
	}


this->place(xi,yi);
//this->move(xi,yi);
}


piece * snake::getHead()
{
	return body.begin();
}


int snake::getEatedTotal()
{
	return this->eated;
}


void snake::place(int x, int y)
{

	vector<piece>::iterator pecas;

//	this->remove();
	
	pecas = body.begin();

	(pecas)->place(x, y);

/*	int varx = 0;
	int vary = 0;

	int xant = pecas->getX();
	int yant = pecas->getY();

	
	if(pecas->getIcon() == FACE_LEFT) varx=-1;
	if(pecas->getIcon() == FACE_RIGHT) varx=1;

	if(pecas->getIcon() == FACE_DOWN) vary=-1;
	if(pecas->getIcon() == FACE_UP) vary=1;

	xant+=varx;
	yant+=vary;
*/
	for(pecas = body.begin()+1; pecas != body.end(); pecas++)
	{
		pecas->place(pecas->getX(), pecas->getY());

/*		pecas->place(xant, yant);
	
		if(pecas->getIcon() == FACE_LEFT) varx=-1;
		if(pecas->getIcon() == FACE_RIGHT) varx=1;

		if(pecas->getIcon() == FACE_DOWN) vary=-1;
		if(pecas->getIcon() == FACE_UP) vary=1;

		xant = pecas->getX()+varx;
		yant = pecas->getY()+vary;

*/	}


}

void snake::setTabuleiro(snaketab *tab)
{

	vector<piece>::iterator pecas;

	for(pecas = body.begin(); pecas!=body.end(); pecas++)
	{
		(pecas)->setTabuleiro(tab);

	}

	this->ground = tab;

}


snaketab * snake::getTabuleiro()
{

	return this->ground;

}



snake::~snake()
{


}


void snake::remove()
{
	vector<piece>::iterator pecas;



	for(pecas = body.begin(); pecas != body.end(); pecas++)
	{
		piece temp = *pecas;
		
		temp.getTabuleiro()->removeAt(temp.getX(), temp.getY());
	}




}


void snake::IncreaseBody()
{



	piece rabo = body.back();

	body.pop_back();

	piece novo_rabo = body.back();


	int varx = 0;
	int vary = 0;


//	int xant = rabo.getX();
//	int yant = rabo.getY();

	if(rabo.getIcon() == FACE_LEFT) varx=-1;
	if(rabo.getIcon() == FACE_RIGHT) varx=+1;

	if(rabo.getIcon() == FACE_DOWN) vary=-1;
	if(rabo.getIcon() == FACE_UP) vary=+1;


	int xant = novo_rabo.getX();
	int yant = novo_rabo.getY();

/*		if(novo_rabo.getIcon() == FACE_LEFT) varx=-1;
		if(novo_rabo.getIcon() == FACE_RIGHT) varx=1;

		if(novo_rabo.getIcon() == FACE_DOWN) vary=-1;
		if(novo_rabo.getIcon() == FACE_UP) vary=1;


	if(novo_rabo.getStatus() == CORPOL)
	{
		if(novo_rabo.getIcon() == FACE_LEFT) varx=1;
		if(novo_rabo.getIcon() == FACE_RIGHT) varx=-1;

		if(novo_rabo.getIcon() == FACE_DOWN) vary=+1;
		if(novo_rabo.getIcon() == FACE_UP) vary=-1;
	}

*/
	xant+=varx;
	yant+=vary;


	piece novo;
	novo.setTabuleiro(rabo.getTabuleiro());
	novo.setIcon(rabo.getIcon());
	novo.setStatus(CORPO);
	novo.setX(xant);
	novo.setY(yant);


	body.push_back(novo);

	body.push_back(rabo);	

	this->place(getX(), getY());
}



bool snake::moveFront()
{

	int x = body.begin()->getX();
	int y = body.begin()->getY();

	switch(body.begin()->getIcon())
	{
		case FACE_LEFT: x++; break;
		case FACE_RIGHT: x--; break;
		case FACE_UP: y--; break;
		case FACE_DOWN: y++; break;

	}


	return move(x,y);

}


bool snake::move(int x, int y)
{

	vector<piece>::iterator point;


	
	int xold = body.begin()->getX();
	int yold = body.begin()->getY();


	if(this->canEat(x,y))
			this->EatApple(x,y);


	for(point = body.begin(); point != body.end(); point ++)
	{
 		
		
		piece *e = (point);

		int xnew = xold;
		int ynew = yold;
		xold = e->getX();
		yold = e->getY();


		switch(e->getStatus())
		{
		case CABECA:		// se for a cabeca...
			if(e->move(x,y) == false)
			{
				return false; // nao moveu!
			}
			if(e->getX() > xold) // moveu para a direita
				e->setIcon(FACE_LEFT);
			if(e->getX() < xold) // para esquerda
				e->setIcon(FACE_RIGHT);
			if(e->getY() > yold) // baixo
				e->setIcon(FACE_DOWN);
			if(e->getY() < yold) // cima
				e->setIcon(FACE_UP);
			
			break;
		
		case CAUDA: // e se for a cauda??
				e->move(xnew, ynew);
				e->setStatus(CAUDA);

				if(e->getX() > xold) // moveu para a direita
					e->setIcon(FACE_LEFT);
				if(e->getX() < xold) // para esquerda
					e->setIcon(FACE_RIGHT);
				if(e->getY() > yold) // baixo
					e->setIcon(FACE_DOWN);
				if(e->getY() < yold) // cima
					e->setIcon(FACE_UP);
				break;
		default:

			e->move(xnew, ynew);

		break;
		} // fim do switch


	
	}	// fim do for




	for(point = body.begin()+1; point != body.end()-1; point ++)
	{
		
		piece *atual = point;
		int yatual = (*point).getY();
		int xatual = (*point).getX();

		piece *next = (point+1); // peca de tras (proxima)
		piece *ant = (point-1);	 // peca da frente (anterior)




		if(ant->getY() == atual->getY())
		{
			if(ant->getX() < atual->getX())
			{
				if(next->getX() < atual->getX())
				{
					atual->setIcon(FACE_LEFT);
					atual->setStatus(CORPO);
				}
				if(next->getX() == atual->getX())
				{
					if(next->getY() > atual->getY())
					{
						atual->setIcon(FACE_DOWN);
						atual->setStatus(CORPOL);
					}
					if(next->getY() < atual->getY())
					{
						atual->setStatus(CORPOL);
						atual->setIcon(FACE_UP);
					}
				}
			}
			if(ant->getX() > atual->getX())
			{
				if(next->getX() < atual->getX())
				{
					atual->setStatus(CORPO);
					atual->setFacing(FACE_RIGHT);
				}
				if(next->getX() == atual->getX())
				{
					if(next->getY() > atual->getY())
					{
						atual->setStatus(CORPOL);
						atual->setFacing(FACE_DOWN);
					}
					if(next->getY() < atual->getY())
					{
						atual->setStatus(CORPOL);
						atual->setFacing(FACE_DOWN);
					}
				}
			}
		}
		if(ant->getY() > atual->getY())
		{
				if(ant->getX() == atual->getX())
				{
					if(next->getX() > atual->getX())
					{
						atual->setStatus(CORPOL);
						atual->setFacing(FACE_DOWN);
					}
					if(next->getX() < atual->getX())
					{
						atual->setStatus(CORPOL);
						atual->setFacing(FACE_LEFT);
					}
					if(next->getX() == atual->getX())
					{
						atual->setFacing(FACE_DOWN);
						atual->setStatus(CORPO);
					}
				}
		}
		if(ant->getY() < atual->getY())
		{
			if(ant->getX() == atual->getX())
			{
				if(next->getX() > atual->getX())
				{
					atual->setStatus(CORPOL);
					atual->setFacing(FACE_RIGHT);
				}
				if(next->getX() < atual->getX())
				{
					atual->setStatus(CORPOL);
					atual->setFacing(FACE_LEFT);
				}
				if(next->getX() == atual->getX())
				{
					atual->setStatus(CORPO);
					atual->setFacing(FACE_UP);
				}
			}
		}

	}


		return true;
}


int snake::getX()
{

	vector<piece>::iterator it;

	it = body.begin();

	return (it)->getX();

}



int snake::getY()
{
	vector<piece>::iterator it;

	it = body.begin();

	return (it)->getY();
}


void snake::Kill()
{
	vector<piece>::iterator pecas;
	
	for(pecas = body.begin(); pecas != body.end(); pecas++)
	{
		piece temp = *pecas;
		
		temp.getTabuleiro()->removeAt(temp.getX(), temp.getY());
	}
}


bool snake::canEat(int x, int y)
{

	return(getTabuleiro()->IsAppleAt(x, y));

}


bool snake::canEat()
{

	piece cabeca = *(body.begin());

			
	if(cabeca.getIcon() == FACE_LEFT) // moveu p/ direita
		if(getTabuleiro()->IsAppleAt(cabeca.getX()+1, cabeca.getY()))
			return true;

	if(cabeca.getIcon() == FACE_RIGHT) // p/ esquerda
		if(getTabuleiro()->IsAppleAt(cabeca.getX()-1, cabeca.getY()))
			return true;


	if(cabeca.getIcon() == FACE_DOWN) // p/ baixo
		if(getTabuleiro()->IsAppleAt(cabeca.getX(), cabeca.getY()+1))
			return true;


	if(cabeca.getIcon() == FACE_UP) // p/ cima
		if(getTabuleiro()->IsAppleAt(cabeca.getX(), cabeca.getY()-1))
			return true;


		return false;
}


void snake::EatApple(int x, int y)
{
	getTabuleiro()->removeApple(x, y);

	IncreaseBody();
	this->eated++;
}


void snake::EatApple()
{
	piece cabeca = *body.begin();

			
	if(cabeca.getIcon() == FACE_LEFT) // moveu p/ direita
	{
		getTabuleiro()->removeApple(cabeca.getX()+1, cabeca.getY());
		this->move(cabeca.getX()+1, cabeca.getY());
		this->IncreaseBody();
	}

	if(cabeca.getIcon() == FACE_RIGHT) // p/ esquerda
	{
		getTabuleiro()->removeApple(cabeca.getX()-1, cabeca.getY());
		this->move(cabeca.getX()-1, cabeca.getY());
		this->IncreaseBody();
	}



	if(cabeca.getIcon() == FACE_DOWN) // p/ baixo
	{
		getTabuleiro()->removeApple(cabeca.getX(), cabeca.getY()+1);
		this->move(cabeca.getX(), cabeca.getY()+1);
		this->IncreaseBody();
	}
			


	if(cabeca.getIcon() == FACE_UP) // p/ cima
	{
		getTabuleiro()->removeApple(cabeca.getX(), cabeca.getY()-1);
		this->move(cabeca.getX(), cabeca.getY()-1);
		this->IncreaseBody();

	}

	eated++;


}


// classe snaketab
//
//////////////////////////////////////////////////////////////////////



snaketab::snaketab(int minmacas)
{

	this->setCircular(false);

	this->min_macas = minmacas;

	macas = 0;

	srand( (unsigned)time( NULL ) );
}


snaketab::~snaketab()
{


}


void snaketab::setApple()
{



   //int i = DEFSIZEX;
   //int j = DEFSIZEY;

  //while(i>=DEFSIZEX)
	int	i = rand()%DEFSIZEX;

  //while(j>=DEFSIZEY)
	int	j = rand()%DEFSIZEY;


	
  while(this->PosicaoOcupada(i,j))
  {

	  srand( (unsigned)time( NULL ) );

	  
        i = rand()%DEFSIZEX;

	  
		j = rand()%DEFSIZEY;


  }
	
  this->tab[i][j] = APPLE;

  macas++;



}



void snaketab::removeApple(int x, int y)
{
	if(this->tab[x][y] == APPLE)
	{
		this->tab[x][y] = FREE;
		macas--;
	}
}


bool snaketab::IsAppleAt(int x, int y)
{
		if(this->tab[x][y] == APPLE)
			return true;

		return false;
}


void snaketab::checkHarvest()
{

	while(macas<min_macas)
		this->setApple();

}

