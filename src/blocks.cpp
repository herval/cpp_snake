#include "blocks.h"

// piece.cpp: implementation of the piece class.
//
//////////////////////////////////////////////////////////////////////

piece::piece()
{

	this->setStatus(FREE);
	this->icon = 0;

	facing = 0;

}

piece::~piece()
{
view = NULL;
}


int piece::getStatus()
{
	return this->status;
}

void piece::setStatus(int novo)
{

	status = novo;

}

void piece::setTabuleiro(tabuleiro *tab)
{

	this->view = tab;

}


tabuleiro * piece::getTabuleiro()
{

	return this->view;
}



void piece::place(int ax, int ay)
{

if(getTabuleiro()->isCircular() == false)
{

	if(ax<0) 
		ax=0;

	if(ax>=DEFSIZEX) 
		ax=DEFSIZEX-1;

	if(ay<0) 
		ay=0;
	
	if(ay>=DEFSIZEY) 
		ay=DEFSIZEY-1;

	this->setX(ax);
	this->setY(ay);

}

else
{
	if(ax<0) 
			ax = DEFSIZEX-1-ax;

	if(ax>DEFSIZEX) 
			ax = 0+(ax-DEFSIZEX);

	if(ay<0) 
			ay=DEFSIZEY-1-ay;
	
	if(ay>DEFSIZEY) 
			ay = 0+(ay-DEFSIZEY);


	this->setX(ax);
	this->setY(ay);
}

	this->getTabuleiro()->place(this);


}




void piece::setIcon(int x)
{
	icon = x;
}

int piece::getIcon()
{

	return this->icon;
}


bool piece::move(int length) // move com angulo
{

	double ang = getFacing(); //*PI)/180;	// angulo = (facing * pi)/180 radianos

	//double m = tan(ang); // coef. angular m = tg(angulo)

//	double coef = 1*length;

//	if(length == 1) coef = 1.05;

	
//	while(length){
	double cosx = cos(ang);
	double sinx = sin(ang);

	double xdest = (double)getX();
	double ydest = (double)getY();

	xdest+=(double)(length*cosx);
	ydest+=(double)(length*sinx);
	/*if((cosx) >= 0.6)
		xdest+=ceil(length*cosx);
	if((cosx) < 0.6)
		xdest+=floor(length*cosx);

	if((sinx) >= 0.6)
		ydest+=ceil(length*sinx);
	
	if((sinx) < 0.6)
		ydest+=floor(length*sinx);
*/
	//this->Line(getX(), getY(), xdest, ydest);
	return(this->BresLineMove(getX(), getY(), xdest, ydest));
	
//	if(this->move(xdest,ydest) == false) return false;
//	length--;
//	}

	
}



void piece::setFacing(float ang)
{

	if(ang >= 6.2831) ang = 0;
	if(ang <= -6.2831) ang = 0;

	facing = ang;
}


double piece::getFacing()
{
return facing;
}


bool piece::move(int quantX, int quantY)
{

	this->getTabuleiro()->remove(this);


if(this->getTabuleiro()->isCircular() == false)
{
	if(quantX<0)
	{
			this->place(this->getX(), this->getY());
			return false;
	}
	 
	if(quantX>=DEFSIZEX)
	{
			this->place(this->getX(), this->getY());
			return false;
	}
	

	if(quantY<0)
	{
		this->place(this->getX(), this->getY());
		return false;

	}
	

	if(quantY>=DEFSIZEY)
	{
			this->place(this->getX(), this->getY());
			return false;
	}

}
else
{

	if(quantX<=0)
	{
		this->place(DEFSIZEX+quantX, this->getY());
		return true;
	}

	if(quantX>=DEFSIZEX)
	{
		this->place(0+quantX-DEFSIZEX, this->getY());
		return true;
	}


	if(quantY<=0)
	{
		this->place(this->getX(), DEFSIZEY+quantY);
		return true;
	}
	

	if(quantY>=DEFSIZEY)
	{
			this->place(getX(), 0+quantY-DEFSIZEY);
			return true;
	}

}


	if(this->view->PosicaoOcupada(quantX, quantY)) 
		{
		this->place(this->getX(), this->getY());
		return false;
		}
	else {
		this->place(quantX, quantY);
		return true;
		}
	
}


int piece::getY()
{
	return this->y;
}


int piece::getX()
{
	return this->x;
}


void piece::setX(int xi)
{
	this->x = xi;
}


void piece::setY(int yi)
{
	this->y = yi;
}


bool piece::Line(int x1, int y1, int x2, int y2)
{
int i, deltax, deltay, numpixels, d, dinc1, dinc2, x, xinc1, xinc2, y, yinc1, yinc2;


//{ Calculate deltax and deltay for initialisation }
deltax = abs(x2 - x1);
deltay = abs(y2 - y1);

//{ Initialize all vars based on which is the independent variable }
if (deltax >= deltay)
  {
  //{ x is independent variable }
  numpixels = deltax + 1;
  d = (2 * deltay) - deltax;
  dinc1 = deltay << 1;
  dinc2 = (deltay - deltax) << 1;
  xinc1 = 1;
  xinc2 = 1;
  yinc1 = 0;
  yinc2 = 1;
  }
else
{
  //{ y is independent variable }
  numpixels = deltay + 1;
  d = (2 * deltax) - deltay;
  dinc1 = deltax << 1;
  dinc2 = (deltax - deltay) << 1;
  xinc1 = 0;
  xinc2 = 1;
  yinc1 = 1;
  yinc2 = 1;
  }

//{ Make sure x and y move in the right directions }
if (x1 > x2)
{
xinc1 = - xinc1;
xinc2 = - xinc2;
}
if(y1 > y2)
{
yinc1 = - yinc1;
yinc2 = - yinc2;
}

//{ Start drawing at <x1, y1> }
x = x1;
y = y1;

//{ Draw the pixels }
for (i= 1; i<=numpixels; i++)
{
move(x, y);
if (d < 0) 
  {
  d = d + dinc1;
  x = x + xinc1;
  y = y + yinc1;
  }
else
  {
  d = d + dinc2;
  x = x + xinc2;
  y = y + yinc2;
  }
}

return true;
}


bool piece::BresLineMove(double Ax, double Ay, double Bx, double By)
{
	//------------------------------------------------------------------------
	// INITIALIZE THE COMPONENTS OF THE ALGORITHM THAT ARE NOT AFFECTED BY THE
	// SLOPE OR DIRECTION OF THE LINE
	//------------------------------------------------------------------------
	int dX = abs(Bx-Ax);	// store the change in X and Y of the line endpoints
	int dY = abs(By-Ay);
	
	//------------------------------------------------------------------------
	// DETERMINE "DIRECTIONS" TO INCREMENT X AND Y (REGARDLESS OF DECISION)
	//------------------------------------------------------------------------
	int Xincr, Yincr;
	if (Ax > Bx) { Xincr=-1; } else { Xincr=1; }	// which direction in X?
	if (Ay > By) { Yincr=-1; } else { Yincr=1; }	// which direction in Y?
	
	//------------------------------------------------------------------------
	// DETERMINE INDEPENDENT VARIABLE (ONE THAT ALWAYS INCREMENTS BY 1 (OR -1) )
	// AND INITIATE APPROPRIATE LINE DRAWING ROUTINE (BASED ON FIRST OCTANT
	// ALWAYS). THE X AND Y'S MAY BE FLIPPED IF Y IS THE INDEPENDENT VARIABLE.
	//------------------------------------------------------------------------
	if (dX >= dY)	// if X is the independent variable
	{           
		int dPr 	= dY<<1;           // amount to increment decision if right is chosen (always)
		int dPru 	= dPr - (dX<<1);   // amount to increment decision if up is chosen
		int P 		= dPr - dX;  // decision variable start value

		for (; dX>=0; dX--)            // process each point in the line one at a time (just use dX)
		{
			if(!this->move(Ax, Ay)) return false;		// move
			//this->move(Ax+getX(), Ay+getY());
			if (P > 0)               // is the pixel going right AND up?
			{ 
				Ax+=Xincr;	       // increment independent variable
				Ay+=Yincr;         // increment dependent variable
				P+=dPru;           // increment decision (for up)
			}
			else                     // is the pixel just going right?
			{
				Ax+=Xincr;         // increment independent variable
				P+=dPr;            // increment decision (for right)
			}
		}		
	}
	else              // if Y is the independent variable
	{
		int dPr 	= dX<<1;           // amount to increment decision if right is chosen (always)
		int dPru 	= dPr - (dY<<1);   // amount to increment decision if up is chosen
		int P 		= dPr - dY;  // decision variable start value

		for (; dY>=0; dY--)            // process each point in the line one at a time (just use dY)
		{
	//		this->move(Ax+getX(), Ay+getY());
			if(!this->move(Ax, Ay)) return false; // move
			if (P > 0)               // is the pixel going up AND right?
			{ 
				Ax+=Xincr;         // increment dependent variable
				Ay+=Yincr;         // increment independent variable
				P+=dPru;           // increment decision (for up)
			}
			else                     // is the pixel just going up?
			{
				Ay+=Yincr;         // increment independent variable
				P+=dPr;            // increment decision (for right)
			}
		}		
	}		
	
	return true;
}




// block.cpp: implementation of the block class.
//
//////////////////////////////////////////////////////////////////////



block::block()
{

	facing = 0;

	for(int i=0; i<SIZEX; i++)
		for(int j=0; j<SIZEY;j++) 
			{
			this->blocos[i][j].setStatus(FREE);
			this->blocos[i][j].setFacing(facing);

			}
	comprimento = 0;
	altura = 0;

	situation = 0;

}


block::~block()
{

	for(int i=0; i<SIZEX; i++)
		for(int j=0; j<SIZEY;j++) 
			blocos[i][j].~piece();

}


void block::setPiece(int ax, int ay)
{


		if(ax<0) ax=0;
		if(ax>=SIZEX) ax=SIZEX-1;

		if(ay<0) ay=0;
		if(ay>SIZEX) ay=SIZEY-1;


	this->blocos[ax][ay].setStatus(OK);

}


void block::setFacing(double fac)
{

//	if(fac>7) fac = fac - 9;
	while((fac >= 6.283)) {
		if(fac < 0) fac = 6.28318-fac;
		if(fac > 6.283) fac = fac-6.28318;
	}

	facing = fac;

}



bool block::BresBlockMove(int Ax, int Ay, int Bx, int By)
{
	//------------------------------------------------------------------------
	// INITIALIZE THE COMPONENTS OF THE ALGORITHM THAT ARE NOT AFFECTED BY THE
	// SLOPE OR DIRECTION OF THE LINE
	//------------------------------------------------------------------------
	int dX = abs(Bx-Ax);	// store the change in X and Y of the line endpoints
	int dY = abs(By-Ay);
	
	//------------------------------------------------------------------------
	// DETERMINE "DIRECTIONS" TO INCREMENT X AND Y (REGARDLESS OF DECISION)
	//------------------------------------------------------------------------
	int Xincr, Yincr;
	if (Ax > Bx) { Xincr=-1; } else { Xincr=1; }	// which direction in X?
	if (Ay > By) { Yincr=-1; } else { Yincr=1; }	// which direction in Y?
	
	//------------------------------------------------------------------------
	// DETERMINE INDEPENDENT VARIABLE (ONE THAT ALWAYS INCREMENTS BY 1 (OR -1) )
	// AND INITIATE APPROPRIATE LINE DRAWING ROUTINE (BASED ON FIRST OCTANT
	// ALWAYS). THE X AND Y'S MAY BE FLIPPED IF Y IS THE INDEPENDENT VARIABLE.
	//------------------------------------------------------------------------
	if (dX >= dY)	// if X is the independent variable
	{           
		int dPr 	= dY<<1;           // amount to increment decision if right is chosen (always)
		int dPru 	= dPr - (dX<<1);   // amount to increment decision if up is chosen
		int P 		= dPr - dX;  // decision variable start value

		for (; dX>=0; dX--)            // process each point in the line one at a time (just use dX)
		{
			if(!this->move(Ax, Ay)) return false;		// move
			//this->move(Ax+getX(), Ay+getY());
			if (P > 0)               // is the pixel going right AND up?
			{ 
				Ax+=Xincr;	       // increment independent variable
				Ay+=Yincr;         // increment dependent variable
				P+=dPru;           // increment decision (for up)
			}
			else                     // is the pixel just going right?
			{
				Ax+=Xincr;         // increment independent variable
				P+=dPr;            // increment decision (for right)
			}
		}		
	}
	else              // if Y is the independent variable
	{
		int dPr 	= dX<<1;           // amount to increment decision if right is chosen (always)
		int dPru 	= dPr - (dY<<1);   // amount to increment decision if up is chosen
		int P 		= dPr - dY;  // decision variable start value

		for (; dY>=0; dY--)            // process each point in the line one at a time (just use dY)
		{
	//		this->move(Ax+getX(), Ay+getY());
			if(!this->move(Ax, Ay)) return false; // move
			if (P > 0)               // is the pixel going up AND right?
			{ 
				Ax+=Xincr;         // increment dependent variable
				Ay+=Yincr;         // increment independent variable
				P+=dPru;           // increment decision (for up)
			}
			else                     // is the pixel just going up?
			{
				Ay+=Yincr;         // increment independent variable
				P+=dPr;            // increment decision (for right)
			}
		}		
	}		
	
	return true;

}


double block::getFacing()
{
	return facing;
}


int block::getStatus()
{
return this->situation;

}

int block::getLength()
{
	return this->comprimento;

}



void block::setIcon(int x)
{

	for(int i = 0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++) 
			this->blocos[i][j].setIcon(x);


}


void block::setStatus(int x)
{
	for(int i = 0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++) 
			if(blocos[i][j].getStatus() != FREE)
				this->blocos[i][j].setStatus(x);


			this->situation = x;
}




int block::getHeight()
{
	return this->altura;

}

void block::place(int ax, int ay)
{


	if(this->getTabuleiro()->isCircular() == false)
	{
		if(ax<0) ax=0;
		if(ax+getLength()>=DEFSIZEX) ax=DEFSIZEX-getLength();

		if(ay<0) ay=0;
		if(ay+getHeight()>=DEFSIZEY) ay=DEFSIZEY-getHeight();
	}
	else
	{

		if(ax<0) ax=DEFSIZEX+ax;
		
		if(ax+getLength()>=DEFSIZEX) ax=0+getLength();

		if(ay<0) ay=DEFSIZEY+ay;

		if(ay+getHeight()>=DEFSIZEY) ay=getHeight();

	}
	


	
	for(int i = 0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++) 
		{
			if(this->blocos[i][j].getStatus() != FREE)
				//if(this->blocos[i][j].getTabuleiro()->PosicaoOcupada(ax+i, ay+j) == false)
				this->blocos[i][j].place(ax+i, ay+j);
			
/*				{
					for(int g=0; g<=i; g++)
						for(int h=0; h<=j; h++)
							this->blocos[g][h].getTabuleiro()->remove(&blocos[g][h]);

					return false;
				}*/
		}

	this->xpos = ax;
	this->ypos = ay;

//return true;
}



void block::setTabuleiro(tabuleiro *tab)
{

	for(int i=0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++)
			this->blocos[i][j].setTabuleiro(tab);

}


bool block::move(int distance)
{
	double ang = getFacing(); //*PI)/180;	// angulo = (facing * pi)/180 radianos

	//double m = tan(ang); // coef. angular m = tg(angulo)

//	double coef = 1*length;

//	if(length == 1) coef = 1.05;

	
//	while(length){
	double cosx = cos(ang);
	double sinx = sin(ang);

	double xdest = getX();
	double ydest = getY();

	if((cosx) >= 0.6)
		xdest+=ceil(distance*cosx);
	if((cosx) < 0.6)
		xdest+=floor(distance*cosx);

	if((sinx) >= 0.6)
		ydest+=ceil(distance*sinx);
	
	if((sinx) < 0.6)
		ydest+=floor(distance*sinx);


	//this->Line(getX(), getY(), xdest, ydest);
	return (this->BresBlockMove(getX(), getY(), xdest, ydest));
	
//	if(this->move(xdest,ydest) == false) return false;
//	length--;
//	}

	

}




bool block::move(int ax, int ay)
{

int xold = xpos, yold = ypos;

this->remove();

	
		bool blocked = false;


	for(int i = 0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++)
			if(blocked == false)
				if(this->blocos[i][j].getStatus() != FREE) 
				{
					if(this->blocos[i][j].getTabuleiro()->isCircular())
					{
						if(ax < 0)
							ax = DEFSIZEX-i+ax;

						if(ay < 0)
							ay = DEFSIZEY-j+ay;
					}
					
					if(this->blocos[i][j].getTabuleiro()->PosicaoOcupada(ax+i, ay+j) == true)
							blocked = true;
			
				}
		


if(blocked == false)
{

	xpos = ax;
	ypos = ay;

	this->place(ax, ay);
}


if(blocked == true)
{

this->place(xold, yold);

	
		xpos = xold;
		ypos = yold;

		return false;
} // fim do if(blocked)


return true;

}



tabuleiro * block::getTabuleiro()
{

	return this->blocos[0][0].getTabuleiro();
}




void block::setSize()
{
int jant = 0;
int iant = 0;

		for(int i=0; i<SIZEX; i++)
			for(int j=0; j<SIZEY; j++)
				if(blocos[i][j].getStatus() != FREE)
				{
					if (i> iant)
					{
						comprimento = i+1;
						iant = i;

					}
			
					if (j > jant)
					{
						altura = j+1;
						jant = j;
					}
				}
				if (altura == 0) altura++;
				if (comprimento == 0) comprimento++;

}


int block::getX()
{

		return xpos;

}



int block::getY()
{


	return ypos;
/*
bool armazenado = false;

for (int i=0; i<SIZEX; i++)
	for(int j=0; j<SIZEY; j++)
		if(armazenado == false)
			if(this->blocos[i][j].getTabuleiro() != NULL)
			{
				yold = this->blocos[i][j].getY();
				armazenado = true;
			}

			return yold;
*/
}

void block::setPos(int x, int y)
{

	this->xpos = x;
	this->ypos = y;


	for(int j=0; j<SIZEX; j++)
		for(int g=0; g<SIZEY; g++)
		{
			this->blocos[j][g].setX(x+j);
			this->blocos[j][g].setY(y+g);
		}

}


void block::remove()
{

for (int i=0; i<SIZEX; i++)
	for (int j=0; j<SIZEY; j++)
			if(this->blocos[i][j].getStatus() != FREE)
				blocos[i][j].getTabuleiro()->remove(&blocos[i][j]);


}


void block::unsetPiece(int x, int y)
{

	this->blocos[x][y].setStatus(FREE);
	this->blocos[x][y].getTabuleiro()->remove(&blocos[x][y]);

}




// tabuleiro.cpp: implementation of the tabuleiro class.
//
//////////////////////////////////////////////////////////////////////

tabuleiro::tabuleiro(int x, int y)
{
	this->sizex = x;
	this->sizey = y;

	init_tab();

	this->setCircular(false);
}


tabuleiro::~tabuleiro()
{

}


void tabuleiro::place(piece *pec)
{

	
//	if(tab[pec->getX()][pec->getY()] == FREE)
//	{
		tab[pec->getX()][pec->getY()] = pec->getStatus();
//		return true;
//	}

//	else return false;


}


void tabuleiro::init_tab()
{

	for (int i = 0; i<DEFSIZEX; i++)
		for (int j = 0; j<DEFSIZEY; j++)
			tab[i][j] = FREE; 

}



bool tabuleiro::isCircular()
{
	return circular;
}



void tabuleiro::setCircular(bool is)
{
circular = is;
}


void tabuleiro::draw(int xin, int yin)
{

//	init_tab();

	for (int i = yin; i<DEFSIZEY; i++) {
		for (int j = xin; j<DEFSIZEX; j++)
				cout<<this->tab[j][i];
		cout<<endl;
	}


/*	piece *temp;

	vector<piece *>::iterator seek;
	for(seek = eixo.begin(); seek !=eixo.end(); seek++)
		{
		temp = *seek;

		tab[temp->getX()][temp->getY()] = temp->Icon();

		}


	for (int i = yin; i<DEFSIZEY; i++) {
		for (int j = xin; j<DEFSIZEX; j++)
				cout<<this->tab[j][i];
		cout<<endl;
				}
			
*/

}


void tabuleiro::removeAt(int x, int y)
{
	tab[x][y] = FREE;
}



bool tabuleiro::PosicaoOcupada(int x, int y)
{

if(this->isCircular() == false)
{

	if(x>=DEFSIZEX) return true;
	if(y>=DEFSIZEY) return true;

	if(x<0) 
	{
			return true;
	}

	if(y<0) 
	{
			return true;
	}

	if(tab[x][y] == FREE) return false;

	else return true;

}
else
{
	if(x>=DEFSIZEX) x = x-DEFSIZEX;
	if(y>=DEFSIZEY) y = y-DEFSIZEY;

	if(x<0) x=DEFSIZEX-x;
	
	if(y<0) y=DEFSIZEY-y;
	

	if(tab[x][y] == FREE) return false;

	else return true;

}

}



void tabuleiro::remove(piece *pec)
{

	tab[pec->getX()][pec->getY()] = FREE;
/*	piece * temp;

	vector<piece *>::iterator seek;
	for(seek = eixo.begin(); seek !=eixo.end(); seek++)
		{
		temp = *seek;


		if(temp == pec)
//	if(temp->getX() == pec->getX())
//		if (temp->getY() == pec->getY())
//			if (temp->getStatus() == pec->getStatus())
						eixo.erase(seek);
		}
	
*/
}


// framedblock.cpp: implementation of the tabuleiro class.
//
//////////////////////////////////////////////////////////////////////

framedblock::framedblock()
{

	actual_facing = 0;

	
	this->atual = &this->quadro[actual_facing];

}



framedblock::~framedblock()
{
	atual = NULL;
	
	for(int i=0; i<FRAMES; i++)
		quadro[i].~block();


}


void framedblock::setTabuleiro(tabuleiro *tab)
{
	for(int i=0; i<FRAMES; i++)
			quadro[i].setTabuleiro(tab);
}


void framedblock::place(int x, int y)
{

	
	this->atual->place(x,y);
	{

	for (int i = 0; i<FRAMES; i++)
	   if(i != this->actual_facing)
			{
				quadro[i].setPos(x, y);
			}
//	return true;
	}

//	return false;


}

bool framedblock::move(int x, int y)
{

	if(this->atual->move(x,y))
	{
	
	for (int i = 0; i<FRAMES; i++)
	   if(i != this->actual_facing)
			quadro[i].setPos(x, y);
	
	return true;
	}

	return false;
}


int framedblock::getX()
{
	return this->atual->getX();
}


int framedblock::getY()
{
	return this->atual->getY();
}



bool framedblock::rotate()
{


bool blocked = false; // a peca pode girar

atual->remove();


int pos=actual_facing+1;

	if(pos>FRAMES-1) pos = 0;
	if(pos<0) pos = FRAMES-1;


int xp = quadro[pos].getX();
int yp = quadro[pos].getY();

		if(xp<0) xp=0;
		if(xp+quadro[pos].getLength()>=DEFSIZEX) xp=DEFSIZEX-quadro[pos].getLength();

		if(yp<0) yp=0;
		if(yp+quadro[pos].getHeight()>=DEFSIZEY) yp=DEFSIZEY-quadro[pos].getHeight();



for(int i = 0; i<SIZEX; i++)
		for(int j=0; j<SIZEY; j++)
			if(blocked == false)
				if(this->quadro[pos].blocos[i][j].getStatus() != FREE) 
				{
					if(this->quadro[pos].getTabuleiro()->PosicaoOcupada(xp+i, yp+j) == true)
						blocked = true;
				}
	

if(blocked) 
{
	place(atual->getX(), atual->getY());
	return false;
}



		this->atual = &this->quadro[pos];

		actual_facing = pos;


		for(int g=0; g<SIZEX; g++)
			for(int j=0; j<SIZEY; j++)
			{
				if(atual->blocos[g][j].getStatus() != FREE)
				{
				if(atual->blocos[g][j].getX()>DEFSIZEX) 
					{
					place(atual->getX()-1, atual->getY());
					return true;
			
					}
				
				if(atual->blocos[g][j].getX()<0)
					{
					place(atual->getX()+1, atual->getY());
					return true;
					}
				
				if(atual->blocos[g][j].getY()<0)
					{
					place(atual->getX(), atual->getY()+1);
					return true;
					}
				
				if(atual->blocos[g][j].getY()>DEFSIZEY)
					{

					place(atual->getX(), atual->getY()-3);
					return true;
					}
								
				}
			}




			//atual->place(atual->getX(), atual->getY());
		
			place(atual->getX(), atual->getY());

			return true;
}



